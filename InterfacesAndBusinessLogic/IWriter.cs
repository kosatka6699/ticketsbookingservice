﻿namespace InterfacesAndBusinessLogic
{
    public interface IWriter
    {
        void Write(int reservationCode);
    }
}