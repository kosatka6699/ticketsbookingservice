﻿namespace InterfacesAndBusinessLogic
{
    using System;
    using System.Collections.Generic;

    public interface ITicketsBooker
    {
        List<FlightModel> ShowFlights(string start, string finish, DateTime date, int numSeats);

        int AddReservation(int flightId, List<PassengerModel> passengersList, string email, string tel);
    }
}