﻿namespace InterfacesAndBusinessLogic
{
    using System;

    public class ConsoleWriter : IWriter
    {
        public void Write(int reservationCode)
        {
            Console.WriteLine("Броня №{0} успешно зарегистрирована в БД", reservationCode);
        }
    }
}