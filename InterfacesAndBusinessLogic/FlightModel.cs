﻿namespace InterfacesAndBusinessLogic
{
    using System;

    public class FlightModel
    {
        public int FlightId { get; set; }

        public string RouteId { get; set; }

        public string Model { get; set; }

        public string StartCity { get; set; }

        public string StartAirport { get; set; }

        public string FinishCity { get; set; }

        public string FinishAirport { get; set; }

        public DateTimeOffset DepartureTime { get; set; }

        public DateTimeOffset? ArrivalTime { get; set; }

        public TimeSpan TransferTimeApproximate { get; set; }

        public decimal BestPriceUsd { get; set; }

        public int AvailableSeats { get; set; }
    }
}