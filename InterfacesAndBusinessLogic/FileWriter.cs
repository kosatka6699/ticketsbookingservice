﻿namespace InterfacesAndBusinessLogic
{
    using System.IO;

    public class FileWriter : IWriter
    {
        public void Write(int reservationCode)
        {
            using (StreamWriter sw = new StreamWriter("out.txt", true, System.Text.Encoding.Default))
            {
                sw.WriteLine("Броня №{0} успешно зарегистрирована в БД", reservationCode);
            }
        }
    }
}