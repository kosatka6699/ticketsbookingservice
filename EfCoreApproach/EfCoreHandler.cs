﻿namespace EfCoreApproach
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using EfCoreApproach.Model;

    using InterfacesAndBusinessLogic;

    using Microsoft.EntityFrameworkCore;

    public class EfCoreHandler : ITicketsBooker, IDisposable
    {
        private readonly AirlinesContext context;
        private bool disposed = false;

        public EfCoreHandler()
        {
            this.context = new AirlinesContext();
        }

        public List<FlightModel> ShowFlights(string start, string finish, DateTime date, int numSeats)
        {
            var list = this.context.InfoOfFlights.FromSql("show_flights_from_to_when @p0, @p1, @p2, @p3", parameters: new object[] { start, finish, date, numSeats }).ToList();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<InfoOfFlight, FlightModel>();
            });
            var mapper = new Mapper(config);
            return mapper.Map<List<FlightModel>>(list);
        }

        public int AddReservation(int flightId, List<PassengerModel> passengersList, string email, string tel)
        {
            Reservation res = new Reservation { FlightId = flightId, Seats = passengersList.Capacity, EMailAddress = email, TelephoneNumber = tel };
            res = this.context.Add(res).Entity;

            Passenger pas;
            foreach (var item in passengersList)
            {
                pas = new Passenger
                {
                    Gender = item.Gender, FirstName = item.FirstName, LastName = item.LastName,
                    BirthDate = item.BirthDate, PassportNo = item.PassportNo
                };
                this.context.Add(pas);
                this.context.Add(new ReservationPassenger { Reservation = res, Passenger = pas });
            }

            var flight = this.context.Flights.FirstOrDefault(f => f.FlightId == flightId);
            if (flight != null)
            {
                flight.AvailableSeats -= passengersList.Capacity;
            }

            this.context.SaveChanges();
            return res.ReservationId;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
                this.context.Dispose();
            }

            this.disposed = true;
        }
    }
}
