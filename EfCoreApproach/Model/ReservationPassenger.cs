﻿using System;
using System.Collections.Generic;

namespace EfCoreApproach.Model
{
    public partial class ReservationPassenger
    {
        public int ReservationId { get; set; }
        public int PassengerId { get; set; }

        public virtual Passenger Passenger { get; set; }
        public virtual Reservation Reservation { get; set; }
    }
}
