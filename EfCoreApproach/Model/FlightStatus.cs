﻿using System;
using System.Collections.Generic;

namespace EfCoreApproach.Model
{
    public partial class FlightStatus
    {
        public FlightStatus()
        {
            Flights = new HashSet<Flight>();
        }

        public int StatusId { get; set; }
        public string StatusName { get; set; }

        public virtual ICollection<Flight> Flights { get; set; }
    }
}
