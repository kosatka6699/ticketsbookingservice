﻿using System;
using System.Collections.Generic;

namespace EfCoreApproach.Model
{
    public partial class Passenger
    {
        public Passenger()
        {
            ReservationPassengers = new HashSet<ReservationPassenger>();
        }

        public int PassengerId { get; set; }
        public string Gender { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string PassportNo { get; set; }
        public DateTime? PassportExpiryDate { get; set; }

        public virtual ICollection<ReservationPassenger> ReservationPassengers { get; set; }
    }
}
