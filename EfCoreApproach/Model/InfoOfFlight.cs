﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EfCoreApproach.Model
{
    public partial class InfoOfFlight
    {
        public int FlightId { get; private set; }
        public string RouteId { get; private set; }
        public string Model { get; private set; }
        public string StartCity { get; private set; }
        public string StartAirport { get; private set; }
        public string FinishCity { get; private set; }
        public string FinishAirport { get; private set; }
        public DateTimeOffset DepartureTime { get; private set; }
        public DateTimeOffset? ArrivalTime { get; private set; }
        public TimeSpan TransferTimeApproximate { get; private set; }
        public decimal BestPriceUsd { get; private set; }
        public int AvailableSeats { get; private set; }
    }
}
