﻿using System;
using System.Collections.Generic;

namespace EfCoreApproach.Model
{
    public partial class Route
    {
        public Route()
        {
            Flights = new HashSet<Flight>();
        }

        public string RouteId { get; set; }
        public string DepartureAirportCode { get; set; }
        public string ArrivalAirportCode { get; set; }

        public virtual Airport ArrivalAirportCodeNavigation { get; set; }
        public virtual Airport DepartureAirportCodeNavigation { get; set; }
        public virtual ICollection<Flight> Flights { get; set; }
    }
}
