﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace EfCoreApproach.Model
{
    public partial class AirlinesContext : DbContext
    {
        public AirlinesContext()
        {
        }

        public AirlinesContext(DbContextOptions<AirlinesContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Aircraft> Aircrafts { get; set; }
        public virtual DbSet<Airport> Airports { get; set; }
        public virtual DbSet<Flight> Flights { get; set; }
        public virtual DbSet<FlightStatus> FlightStatuses { get; set; }
        public virtual DbSet<Passenger> Passengers { get; set; }
        public virtual DbSet<Reservation> Reservations { get; set; }
        public virtual DbSet<ReservationPassenger> ReservationPassengers { get; set; }
        public virtual DbSet<Route> Routes { get; set; }
        public virtual DbQuery<InfoOfFlight> InfoOfFlights { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=(LocalDb)\\MSSQLLocalDb;Database=Airlines;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Aircraft>(entity =>
            {
                entity.Property(e => e.AircraftId).HasColumnName("AircraftID");

                entity.Property(e => e.Model)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Airport>(entity =>
            {
                entity.HasKey(e => e.AirportCode);

                entity.Property(e => e.AirportCode)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AirportName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Country)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TimeZone)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Flight>(entity =>
            {
                entity.Property(e => e.FlightId).HasColumnName("FlightID");

                entity.Property(e => e.AircraftId).HasColumnName("AircraftID");

                entity.Property(e => e.BestPriceUsd)
                    .HasColumnName("BestPriceUSD")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.RouteId)
                    .IsRequired()
                    .HasColumnName("RouteID")
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId)
                    .HasColumnName("StatusID")
                    .HasDefaultValueSql("((3))");

                entity.HasOne(d => d.Aircraft)
                    .WithMany(p => p.Flights)
                    .HasForeignKey(d => d.AircraftId)
                    .HasConstraintName("FK_Flights_AID");

                entity.HasOne(d => d.Route)
                    .WithMany(p => p.Flights)
                    .HasForeignKey(d => d.RouteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Flights_RID");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Flights)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Flights_SID");
            });

            modelBuilder.Entity<FlightStatus>(entity =>
            {
                entity.HasKey(e => e.StatusId)
                    .HasName("PK_Status");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.Property(e => e.StatusName)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Passenger>(entity =>
            {
                entity.HasIndex(e => e.PassportNo)
                    .HasName("UQ__Passenge__185526FEA9C828DF")
                    .IsUnique();

                entity.Property(e => e.PassengerId).HasColumnName("PassengerID");

                entity.Property(e => e.BirthDate).HasColumnType("date");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PassportExpiryDate).HasColumnType("date");

                entity.Property(e => e.PassportNo)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Reservation>(entity =>
            {
                entity.Property(e => e.ReservationId).HasColumnName("ReservationID");

                entity.Property(e => e.DateReservationExpired)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(dateadd(hour,(3),getdate()))");

                entity.Property(e => e.DateReservationMade)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EMailAddress)
                    .IsRequired()
                    .HasColumnName("E-mailAddress")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FlightId).HasColumnName("FlightID");

                entity.Property(e => e.TelephoneNumber)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.Flight)
                    .WithMany(p => p.Reservations)
                    .HasForeignKey(d => d.FlightId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Reservations");
            });

            modelBuilder.Entity<ReservationPassenger>(entity =>
            {
                entity.HasKey(e => new { e.ReservationId, e.PassengerId });

                entity.Property(e => e.ReservationId).HasColumnName("ReservationID");

                entity.Property(e => e.PassengerId).HasColumnName("PassengerID");

                entity.HasOne(d => d.Passenger)
                    .WithMany(p => p.ReservationPassengers)
                    .HasForeignKey(d => d.PassengerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ReservationPassengers_PID");

                entity.HasOne(d => d.Reservation)
                    .WithMany(p => p.ReservationPassengers)
                    .HasForeignKey(d => d.ReservationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ReservationPassengers_RID");
            });

            modelBuilder.Entity<Route>(entity =>
            {
                entity.HasIndex(e => e.ArrivalAirportCode)
                    .HasName("ix_arrairp");

                entity.HasIndex(e => e.DepartureAirportCode)
                    .HasName("ix_depairp");

                entity.Property(e => e.RouteId)
                    .HasColumnName("RouteID")
                    .HasMaxLength(7)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ArrivalAirportCode)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.DepartureAirportCode)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.HasOne(d => d.ArrivalAirportCodeNavigation)
                    .WithMany(p => p.RouteArrivalAirportCodeNavigations)
                    .HasForeignKey(d => d.ArrivalAirportCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Routes_AAC");

                entity.HasOne(d => d.DepartureAirportCodeNavigation)
                    .WithMany(p => p.RouteDepartureAirportCodeNavigations)
                    .HasForeignKey(d => d.DepartureAirportCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Routes_DAC");
            });

            modelBuilder.Query<InfoOfFlight>().ToView("InfoOfFlights");
        }
    }
}
