﻿using System;
using System.Collections.Generic;

namespace EfCoreApproach.Model
{
    public partial class Reservation
    {
        public Reservation()
        {
            ReservationPassengers = new HashSet<ReservationPassenger>();
        }

        public int ReservationId { get; set; }
        public int FlightId { get; set; }
        public int Seats { get; set; }
        public DateTime? DateReservationMade { get; set; }
        public DateTime? DateReservationExpired { get; set; }
        public string EMailAddress { get; set; }
        public string TelephoneNumber { get; set; }
        public bool Paid { get; set; }

        public virtual Flight Flight { get; set; }
        public virtual ICollection<ReservationPassenger> ReservationPassengers { get; set; }
    }
}
