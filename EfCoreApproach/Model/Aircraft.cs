﻿using System;
using System.Collections.Generic;

namespace EfCoreApproach.Model
{
    public partial class Aircraft
    {
        public Aircraft()
        {
            Flights = new HashSet<Flight>();
        }

        public int AircraftId { get; set; }
        public string Model { get; set; }
        public int MaxSeats { get; set; }

        public virtual ICollection<Flight> Flights { get; set; }
    }
}
