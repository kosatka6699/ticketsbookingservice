﻿using System;
using System.Collections.Generic;

namespace EfCoreApproach.Model
{
    public partial class Airport
    {
        public Airport()
        {
            RouteArrivalAirportCodeNavigations = new HashSet<Route>();
            RouteDepartureAirportCodeNavigations = new HashSet<Route>();
        }

        public string AirportCode { get; set; }
        public string AirportName { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string TimeZone { get; set; }

        public virtual ICollection<Route> RouteArrivalAirportCodeNavigations { get; set; }
        public virtual ICollection<Route> RouteDepartureAirportCodeNavigations { get; set; }
    }
}
