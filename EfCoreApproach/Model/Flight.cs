﻿using System;
using System.Collections.Generic;

namespace EfCoreApproach.Model
{
    public partial class Flight
    {
        public Flight()
        {
            Reservations = new HashSet<Reservation>();
        }

        public int FlightId { get; set; }
        public string RouteId { get; set; }
        public int AircraftId { get; set; }
        public DateTimeOffset DepartureTime { get; set; }
        public TimeSpan TransferTimeApproximate { get; set; }
        public DateTimeOffset? ArrivalTime { get; set; }
        public int StatusId { get; set; }
        public decimal BestPriceUsd { get; set; }
        public int AvailableSeats { get; set; }

        public virtual Aircraft Aircraft { get; set; }
        public virtual Route Route { get; set; }
        public virtual FlightStatus Status { get; set; }
        public virtual ICollection<Reservation> Reservations { get; set; }
    }
}
