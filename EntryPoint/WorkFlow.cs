﻿namespace EntryPoint
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;

    using AdoApproach;

    using EfApproach;

    using EfCoreApproach;

    using InterfacesAndBusinessLogic;

    public class WorkFlow
    {
        private int technology;
        private int output;
        private ITicketsBooker handler;
        private IWriter writer;

        public WorkFlow(int technology, int output)
        {
            this.technology = technology;
            switch (this.technology)
            {
                case 1:
                    this.handler = new AdoHandler();
                    break;
                case 2:
                    this.handler = new EfHandler();
                    DbProviderFactories.RegisterFactory("System.Data.SqlClient", SqlClientFactory.Instance);
                    break;
                case 3:
                    this.handler = new EfCoreHandler();
                    break;
            }

            this.output = output;
            switch (this.output)
            {
                case 1:
                    this.writer = new ConsoleWriter();
                    break;
                case 2:
                    this.writer = new FileWriter();
                    break;
            }
        }

        public List<FlightModel> RequestToGetFlights(string start, string finish, DateTime date, int numSeats)
        {
            return this.handler.ShowFlights(start, finish, date, numSeats);
        }

        public void RequestToBook(int flightId, List<PassengerModel> passengersList, string email, string tel)
        {
            var reservationCode = this.handler.AddReservation(flightId, passengersList, email, tel);
            this.writer.Write(reservationCode);
        }
    }
}