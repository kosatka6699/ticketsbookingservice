﻿namespace EntryPoint
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using InterfacesAndBusinessLogic;

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Выберите технологию:");
            Console.WriteLine("1. ADO.Net");
            Console.WriteLine("2. Entity Framework");
            Console.WriteLine("3. Entity Framework Core");
            int technology = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Выберите поток вывода:");
            Console.WriteLine("1. Консоль");
            Console.WriteLine("2. Файл");
            int output = Convert.ToInt32(Console.ReadLine());

            WorkFlow workFlow = new WorkFlow(technology, output);

            Console.WriteLine("Введите данные рейса");
            Console.WriteLine("Откуда:");
            string start = Console.ReadLine();
            Console.WriteLine("Куда:");
            string finish = Console.ReadLine();
            Console.WriteLine("Когда (в формате ДД.ММ.ГГГГ):");
            DateTime date;
            while (!DateTime.TryParse(Console.ReadLine(), out date))
            {
                Console.WriteLine("Вы ввели некорректную дату. Введите еще раз (в формате ДД.ММ.ГГГГ):");
            }

            Console.WriteLine("Количество мест:");
            int numSeats;
            while (!int.TryParse(Console.ReadLine(), out numSeats))
            {
                Console.WriteLine("Вы ввели некорректное число. Введите еще раз число:");
            }

            var flightsList = workFlow.RequestToGetFlights(start, finish, date, numSeats);
            while (!flightsList.Any())
            {
                Console.WriteLine("На данную дату рейсы не найдены. Введите другую дату (в формате ДД.ММ.ГГГГ):");
                Console.WriteLine("Для выхода из приложения введите Q");
                string tempLine = Console.ReadLine();
                while (!DateTime.TryParse(tempLine, out date))
                {
                    if (tempLine == "Q")
                    {
                        Environment.Exit(0);
                    }
                    else
                    {
                        Console.WriteLine("Вы ввели некорректную дату. Введите еще раз (в формате ДД.ММ.ГГГГ):");
                        tempLine = Console.ReadLine();
                    }
                }

                flightsList = workFlow.RequestToGetFlights(start, finish, date, numSeats);
            }

            foreach (var flight in flightsList)
            {
                Console.WriteLine(
                    "{0,5}| {1} {2}({3}) {4} {5}({6}) {7} {8,20} {9,2} ч {10,2} м {11,7} $ {12,3} мест",
                    flight.FlightId,
                    flight.DepartureTime.TimeOfDay,
                    flight.StartAirport,
                    flight.StartCity,
                    flight.ArrivalTime.Value.TimeOfDay,
                    flight.FinishAirport,
                    flight.FinishCity,
                    flight.RouteId,
                    flight.Model,
                    flight.TransferTimeApproximate.Hours,
                    flight.TransferTimeApproximate.Minutes,
                    flight.BestPriceUsd,
                    flight.AvailableSeats);
            }

            Console.WriteLine("Введите номер рейса из списка:");
            Console.WriteLine("Для выхода из приложения введите Q");
            int flightId;
            string checkLine = Console.ReadLine();
            while (!int.TryParse(checkLine, out flightId) || flightsList.FindIndex(f => f.FlightId == flightId) == -1)
            {
                if (checkLine == "Q")
                {
                    Environment.Exit(0);
                }
                else
                {
                    Console.WriteLine("Вы ввели номер не из списка. Попробуйте еще раз:");
                    checkLine = Console.ReadLine();
                }
            }

            Console.WriteLine("Пожалуйста, введите данные о пассажирах латинскими буквами.");
            List<PassengerModel> passengersList = new List<PassengerModel>(numSeats);
            for (int i = 0; i < numSeats; ++i)
            {
                passengersList.Add(new PassengerModel());
                Console.WriteLine("Пассажир №{0}", i + 1);
                Console.WriteLine("Пол (M или F):");
                passengersList[i].Gender = Console.ReadLine();
                Console.WriteLine("Имя:");
                passengersList[i].FirstName = Console.ReadLine();
                Console.WriteLine("Фамилия:");
                passengersList[i].LastName = Console.ReadLine();
                Console.WriteLine("Дата рождения:");
                passengersList[i].BirthDate = Convert.ToDateTime(Console.ReadLine());
                Console.WriteLine("Номер паспорта:");
                passengersList[i].PassportNo = Console.ReadLine();
            }

            Console.WriteLine("Введите контактные данные для заказа:");
            Console.WriteLine("Эл. почта:");
            var email = Console.ReadLine();
            Console.WriteLine("Телефон:");
            var tel = Console.ReadLine();

            workFlow.RequestToBook(flightId, passengersList, email, tel);
        }
    }
}
