﻿namespace EfApproach
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper;

    using InterfacesAndBusinessLogic;

    public class EfHandler : ITicketsBooker, IDisposable
    {
        private readonly AirlinesEntities context;
        private bool disposed = false;

        public EfHandler()
        {
            this.context = new AirlinesEntities();
        }

        public List<FlightModel> ShowFlights(string start, string finish, DateTime date, int numSeats)
        {
            var list = this.context.show_flights_from_to_when(start, finish, date, numSeats).ToList();
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<show_flights_from_to_when_Result, FlightModel>();
            });
            var mapper = new Mapper(config);
            return mapper.Map<List<FlightModel>>(list);
        }

        public int AddReservation(int flightId, List<PassengerModel> passengersList, string email, string tel)
        {
            Reservation res = new Reservation { FlightID = flightId, Seats = passengersList.Capacity, E_mailAddress = email, TelephoneNumber = tel };
            res = this.context.Reservations.Add(res);

            Passenger pas;
            foreach (var item in passengersList)
            {
                pas = new Passenger
                {
                    Gender = item.Gender,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    BirthDate = item.BirthDate,
                    PassportNo = item.PassportNo
                };
                pas.Reservations.Add(res);
                this.context.Passengers.Add(pas);
            }

            var flight = this.context.Flights.FirstOrDefault(f => f.FlightID == flightId);
            if (flight != null)
            {
                flight.AvailableSeats -= passengersList.Capacity;
            }

            this.context.SaveChanges();
            return res.ReservationID;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
                this.context.Dispose();
            }

            this.disposed = true;
        }
    }
}