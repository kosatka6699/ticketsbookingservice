﻿namespace AdoApproach
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using InterfacesAndBusinessLogic;

    public class AdoHandler : ITicketsBooker, IDisposable
    {
        private readonly SqlConnection connection;
        private bool disposed = false;

        public AdoHandler()
        {
            this.connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionToAirlines"].ConnectionString);
            this.connection.Open();
        }

        public List<FlightModel> ShowFlights(string start, string finish, DateTime date, int numSeats)
        {
            using (SqlCommand getFlights = new SqlCommand("show_flights_from_to_when", this.connection))
            {
                getFlights.CommandType = CommandType.StoredProcedure;

                getFlights.Parameters.Add("@start", SqlDbType.VarChar, 50).Value = start;
                getFlights.Parameters.Add("@finish", SqlDbType.VarChar, 50).Value = finish;
                getFlights.Parameters.Add("@date", SqlDbType.Date).Value = date;
                getFlights.Parameters.Add("@numSeats", SqlDbType.Int).Value = numSeats;

                using (SqlDataReader flightsDataReader = getFlights.ExecuteReader())
                {
                    List<FlightModel> list = new List<FlightModel>();
                    if (!flightsDataReader.HasRows)
                    {
                        return list;
                    }

                    FlightModel flight;
                    while (flightsDataReader.Read())
                    {
                        flight = new FlightModel
                                     {
                                         FlightId = flightsDataReader.GetInt32(0),
                                         RouteId = flightsDataReader.GetString(1),
                                         Model = flightsDataReader.GetString(2),
                                         StartCity = flightsDataReader.GetString(3),
                                         StartAirport = flightsDataReader.GetString(4),
                                         FinishCity = flightsDataReader.GetString(5),
                                         FinishAirport = flightsDataReader.GetString(6),
                                         DepartureTime = flightsDataReader.GetDateTimeOffset(7),
                                         ArrivalTime = flightsDataReader.GetDateTimeOffset(8),
                                         TransferTimeApproximate = flightsDataReader.GetTimeSpan(9),
                                         BestPriceUsd = flightsDataReader.GetDecimal(10),
                                         AvailableSeats = flightsDataReader.GetInt32(11)
                                     };
                        list.Add(flight);
                    }

                    return list;
                }
            }
        }

        public int AddReservation(int flightId, List<PassengerModel> passengersList, string email, string tel)
        {
            DataTable pasIDs = new DataTable();
            DataColumn id = new DataColumn("ID", typeof(int));
            pasIDs.Columns.Add(id);

            using (SqlCommand insertPassengers = new SqlCommand("INSERT INTO Passengers (Gender, FirstName, LastName, BirthDate, PassportNo) OUTPUT inserted.PassengerID VALUES(@gender, @firstname, @lastname, @birthdate, @passportNo)", this.connection))
            {
                SqlParameter parameter1 = insertPassengers.Parameters.Add("gender", SqlDbType.Char);
                SqlParameter parameter2 = insertPassengers.Parameters.Add("firstname", SqlDbType.VarChar, 50);
                SqlParameter parameter3 = insertPassengers.Parameters.Add("lastname", SqlDbType.VarChar, 50);
                SqlParameter parameter4 = insertPassengers.Parameters.Add("birthdate", SqlDbType.Date);
                SqlParameter parameter5 = insertPassengers.Parameters.Add("passportNo", SqlDbType.VarChar, 15);
                foreach (var pas in passengersList)
                {
                    parameter1.Value = pas.Gender;
                    parameter2.Value = pas.FirstName;
                    parameter3.Value = pas.LastName;
                    parameter4.Value = pas.BirthDate;
                    parameter5.Value = pas.PassportNo;
                    DataRow row = pasIDs.NewRow();
                    row[0] = (int)insertPassengers.ExecuteScalar();
                    pasIDs.Rows.Add(row);
                }
            }

            int reservationId;
            using (var addReservation = new SqlCommand("add_reservation", this.connection))
            {
                addReservation.CommandType = CommandType.StoredProcedure;
                addReservation.Parameters.AddWithValue("@passengers", pasIDs);
                addReservation.Parameters.Add("@fl_ID", SqlDbType.Int).Value = flightId;
                addReservation.Parameters.Add("@asked_seats", SqlDbType.Int).Value = passengersList.Capacity;
                addReservation.Parameters.Add("@email", SqlDbType.VarChar, 50).Value = email;
                addReservation.Parameters.Add("@tel", SqlDbType.VarChar, 20).Value = tel;
                addReservation.Parameters.Add("@payment_confirmation", SqlDbType.Bit).Value = false;

                reservationId = Convert.ToInt32(addReservation.ExecuteScalar());
            }

            return reservationId;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
                this.connection.Dispose();
            }

            this.disposed = true;
        }
    }
}
